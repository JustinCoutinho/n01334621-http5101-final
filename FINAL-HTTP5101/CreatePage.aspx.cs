﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FINAL_HTTP5101
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Create_Page_Btn(object sender, EventArgs e)
        {
            string pagetitle = title.Text.ToString();
            string pagecontent = content.Text.ToString();
            string author = pageauthor.Text.ToString();

            //This query pulls the data from the strings and creates a new page
            string createquery = "insert into pages (pageid, pagetitle, pagecontent, author) " +
                "values ((select max(pageid) from pages) +1, " + "'" + pagetitle + "', '" + pagecontent + "', '" 
                + author + "')";

            //https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient.sqldataadapter.insertcommand?view=netframework-4.7.2
            //assign insert command to query that will create a new page.
            addpage.InsertCommand = createquery;
            addpage.Insert();

           
        }
    }
}