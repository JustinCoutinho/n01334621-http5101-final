﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FINAL_HTTP5101
{
    public partial class _Default : Page
    {    
        private string query = "SELECT pageid, pageTitle AS 'Title', author AS 'Author', pubdate AS 'Publish Date & Time' FROM pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            //connecting sql source to query string above
            create.SelectCommand = query;
            //datagrid calling dataview object and binding sql data source
            pagesList.DataSource = Page_Bind(create);
            pagesList.DataBind();
        }
        protected DataView Page_Bind(SqlDataSource src)
        {
            DataTable tbl;
            DataView dv;

            tbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            //Thank you Christine
            foreach (DataRow row in tbl.Rows)
            {
                //Adds link info from column
                row["Title"] =
                    "<a href=\"ViewPage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Title"]
                    + "</a>";

            }

            tbl.Columns.Remove("pageid");
            dv = tbl.DefaultView;

            return dv;

        }
    }

    
}