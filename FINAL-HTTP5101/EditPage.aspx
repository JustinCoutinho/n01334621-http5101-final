﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FINAL_HTTP5101.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pagetitle" runat="server">
    <h1 runat="server" id="pageinfo">Edit</h1>

    <div runat="server" class="querybox" id="edit_debug"></div>

     <asp:TextBox runat="server" ID="title" placeholder="Title" CssClass="form-control input-md"></asp:TextBox>
        <asp:RegularExpressionValidator ControlToValidate = "title" 
        ID="character_count" ValidationExpression = "^[\s\S]{1,20}$" runat="server" 
        ErrorMessage="Title cannot be more than 20 characters"></asp:RegularExpressionValidator>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="pagecontent" runat="server">

    <asp:TextBox runat="server" ID="author" placeholder="Author" CssClass="form-control input-md"></asp:TextBox>
        <asp:RegularExpressionValidator ControlToValidate = "author" 
        ID="character_count2" ValidationExpression = "^[\s\S]{1,20}$" runat="server" 
        ErrorMessage="Title cannot be more than 20 characters"></asp:RegularExpressionValidator>

    <div>
    <asp:TextBox runat="server" ID="content" placeholder="Content" TextMode="MultiLine" CssClass="form-control input-lg"></asp:TextBox>
        <asp:RegularExpressionValidator ControlToValidate = "content" 
        ID="character_count3" ValidationExpression = "^[\s\S]{1,500}$" runat="server" 
        ErrorMessage="Title cannot be more than 500 characters"></asp:RegularExpressionValidator>
    </div>

    <asp:SqlDataSource runat="server" ID="editpage" ConnectionString="<%$ ConnectionStrings:create_table %>">
    </asp:SqlDataSource>

    <asp:Button runat="server" Text="Save" ID="edit_btn" OnClick="Edit_Page_Btn" UseSubmitBehavior="true" CssClass="btn" BackColor="Black"></asp:Button>

</asp:Content>
