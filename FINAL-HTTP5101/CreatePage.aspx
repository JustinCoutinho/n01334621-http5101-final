﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreatePage.aspx.cs" Inherits="FINAL_HTTP5101.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagetitle" runat="server">
    
    <h1>Create a Page</h1>

    <asp:TextBox runat="server" ID="title" placeholder="Title" CssClass="form-control input-md"></asp:TextBox>

    <asp:RegularExpressionValidator ControlToValidate = "title" 
        ID="character_count" ValidationExpression = "^[\s\S]{1,20}$" runat="server" 
        ErrorMessage="Title cannot be more than 20 characters">

    </asp:RegularExpressionValidator>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="pagecontent" runat="server">
    
<asp:SqlDataSource
    runat="server"
    ID="addpage"
    ConnectionString="<%$ ConnectionStrings:create_table %>">
</asp:SqlDataSource>

<div>
    <asp:TextBox runat="server" ID="pageauthor" placeholder="Author" CssClass="form-control input-md"></asp:TextBox>
    
    <asp:RegularExpressionValidator ControlToValidate = "pageauthor" 
        ID="character_count2" ValidationExpression = "^[\s\S]{1,20}$" runat="server" 
        ErrorMessage="Title cannot be more than 20 characters"></asp:RegularExpressionValidator>
</div>

    <asp:TextBox runat="server" ID="content" placeholder="Content" TextMode="MultiLine" CssClass="form-control input-lg"></asp:TextBox>
        <asp:RegularExpressionValidator ControlToValidate = "content" 
        ID="character_count3" ValidationExpression = "^[\s\S]{1,500}$" runat="server" 
        ErrorMessage="Title cannot be more than 500 characters"></asp:RegularExpressionValidator>

<div>
<asp:Button runat="server" Text="Create Page" ID="create_btn" OnClick="Create_Page_Btn" CssClass="btn" BackColor="Black"></asp:Button>
</div>
</asp:Content>
