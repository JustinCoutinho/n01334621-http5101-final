﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FINAL_HTTP5101
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string pages_query = "SELECT pagetitle, pagecontent, author, pubdate FROM pages";

        private string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        //Referenced from Christine's student example
        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null) pagetitle.InnerHtml = "No page data";
            else

            {

                pages_query += " WHERE PAGEID = " + pageid;
                create.SelectCommand = pages_query;


                DataView pagesview = (DataView)create.Select(DataSourceSelectArguments.Empty);

                DataRowView pagerowview = pagesview[0];

                //['attribute name'] is the column where info is being collected 
                string pagename = pagerowview["pagetitle"].ToString();
                pagetitle.InnerHtml = pagename;

                string author = pagerowview["author"].ToString();
                pageauthor.InnerHtml = "Author: " + author;

                string content = pagerowview["pagecontent"].ToString();
                pagecontent.InnerHtml = content;


            }
        }
            protected void Del_Page(object sender, EventArgs e)
            {
                string deletePage = "DELETE FROM pages " +
                    "WHERE pageid = " + pageid;
                
            //prints query to page so you can see what's happening 
                del_debug.InnerHtml = deletePage;
                delete.DeleteCommand = deletePage;
                delete.Delete();

            }

            protected void Edit_Page(object sender, EventArgs e)
            {

            string editURL = "EditPage.aspx?pageid=" + pageid;
            Server.Transfer("EditPage.aspx?pageid=" + pageid);

            }
        
    }
}