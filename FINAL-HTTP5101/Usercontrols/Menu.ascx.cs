﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FINAL_HTTP5101.Usercontrols
{
    public partial class Menu : System.Web.UI.UserControl
    {
        

        private string pages_query = "select pageid, pagetitle from pages";

        private int page_id;

        public int _pageid
        {
            get { return page_id; }
            set { page_id = value; }
        }

        

        protected void Page_Load(object sender, EventArgs e)
        {
            menusrc.SelectCommand = pages_query;
            Page_Bind(menusrc, "menu");

        }

       
        void Page_Bind(SqlDataSource src, string menu_id)
        {
            
            DataView dv = (DataView)src.Select(DataSourceSelectArguments.Empty);
            //Thank you Christine
            foreach (DataRowView row in dv)
            {
                
                string pagetitle = row["pagetitle"].ToString();
                string pagelink = "ViewPage.aspx?pageid=" + row["pageid"];
                menuitems.InnerHtml += "<li><a href=\""+pagelink+"\">"+pagetitle+"</a></li>";

            }
                    
        }
      





    }



}


