﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="FINAL_HTTP5101._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagetitle" runat="server">
    
    <h1>Manage Pages</h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="pagecontent" runat="server">


<asp:SqlDataSource
    runat="server"
    ID="create"
    ConnectionString="<%$ ConnectionStrings:create_table %>">
</asp:SqlDataSource>


    <asp:DataGrid ID="pagesList" runat="server" CssClass="table"></asp:DataGrid>

<a href="/CreatePage">Create Page</a>
</asp:Content> 

