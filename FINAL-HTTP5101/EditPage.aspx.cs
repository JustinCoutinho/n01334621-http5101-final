﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FINAL_HTTP5101
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        protected DataRowView get_page_data(int id)
        {
            string getQuery = "SELECT * FROM pages WHERE pageid = " + pageid.ToString();
            editpage.SelectCommand = getQuery;

            DataView pageview = (DataView)editpage.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];

            return pagerowview;

        }
        //Thank you Christine
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView pagerow = get_page_data(pageid);
            if (pagerow == null)
            {
                pageinfo.InnerHtml = "Nothing here (ツ)";
                return;
            } 
           
            title.Text = pagerow["pagetitle"].ToString();

            author.Text = pagerow["author"].ToString();

            content.Text = pagerow["pagecontent"].ToString();

        }


        protected void Edit_Page_Btn(object sender, EventArgs e)
        {
            string ptitle = title.Text;
            string pcontent = content.Text;
            string pauthor = author.Text;
            //https://forums.asp.net/t/1346477.aspx?Can+t+remove+apostrophe+from+string
             ptitle = ptitle.Replace("'", "''");
             pcontent = pcontent.Replace("'", "''");
             pauthor = pauthor.Replace("'", "''");


            string edit_query = "UPDATE pages SET pagetitle = '" + ptitle + "'," +
                " pagecontent = '" + pcontent + "', " + " author = '" + pauthor + "' " +
                "where pageid =" + pageid;
            edit_debug.InnerHtml = edit_query;

           
            editpage.UpdateCommand = edit_query;
            editpage.Update();
        }

        
    }
}