﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" Inherits="FINAL_HTTP5101.WebForm1" %>
 <asp:Content id="title" ContentPlaceHolderID="pagetitle" runat="server">

     <h1 runat="server" id="pagetitle"></h1>
     <h2 runat="server" id="pageauthor"></h2>


 </asp:Content>

 <asp:Content id="content" ContentPlaceHolderID="pagecontent" runat="server">


     <p runat="server" id="pagecontent">content goes here</p>
     
     <asp:SqlDataSource runat="server" ID="create" ConnectionString="<%$ ConnectionStrings:create_table %>">
     </asp:SqlDataSource>

     <asp:SqlDataSource runat="server" ID="delete" ConnectionString="<%$ ConnectionStrings:create_table %>">
     </asp:SqlDataSource>

     <div id="del_debug" class="querybox" runat="server"></div>

        <asp:Button runat="server" id="deleteBtn" OnClick="Del_Page" CssClass="btn"
        OnClientClick="if(!confirm('Are you sure you want to delete this page?')) return false;"
        Text="Delete" BackColor="Black" />

        <asp:Button runat="server" id="editBtn" OnClick="Edit_Page" Text="Edit" BackColor="Black" CssClass="btn" />

 </asp:Content>